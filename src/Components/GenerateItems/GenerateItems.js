import React, { useState } from 'react';
import Button from '../Button/Button';
import Heading from '../Heading/Heading';
import data from './../../Data/loremIpsum';

const GenerateItems = () => {
    const [text, setText] = useState([]);
    const [count, setCount] = useState(0);

    const handleSubmit = (e) => {
        e.preventDefault();
        let amount = parseInt(count);
        if (count <= 0) {
            amount = 1;
        }
        if (count > 8) {
            amount = 8;
        }
        setText(data.slice(0, amount));
    };
    const handleChangeValue = (e) => {
        setCount(e.target.value);
    };

    return (
        <div className='flex flex-col items-center'>
            <Heading>Generate Lorem Inpsum?</Heading>
            <div className='flex'>
                <span className='mr-2'>Paragraph:</span>
                <form onSubmit={handleSubmit}>
                    <input
                        className='py-1'
                        type='number'
                        value={count}
                        onChange={handleChangeValue}
                    />
                    <Button type='submit' className='px-3 py-1'>
                        GENERATE
                    </Button>
                </form>
            </div>
            <article>
                {text.map((item, index) => (
                    <p key={index} className='max-w-2xl my-5 text-gray-700'>
                        {item}
                    </p>
                ))}
            </article>
        </div>
    );
};

export default GenerateItems;
