import React, { useEffect } from 'react';

const Alert = ({ msg, type, removeAlert, list }) => {
    useEffect(() => {
        const time = setTimeout(() => {
            removeAlert();
        }, 3000);
        return () => clearTimeout(time);
    }, [list, removeAlert]);
    return (
        <p
            className={`px-3 py-1 w-full text-center my-3 rounded-lg ${
                type === 'danger'
                    ? 'bg-red-600 text-gray-50'
                    : 'bg-green-400 text-gray-100'
            }`}
        >
            {msg}
        </p>
    );
};

export default Alert;
