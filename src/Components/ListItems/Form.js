import React from 'react';
import Button from '../Button/Button';

const Form = ({ text, handleChangeText, handleSubmit, isEditing }) => {
    return (
        <form onSubmit={handleSubmit} className='w-full flex my-6'>
            <input
                className='border px-2 w-auto h-10 flex-1'
                type='text'
                value={text}
                onChange={handleChangeText}
                placeholder='Clean the house'
            />
            <Button type='submit' className='px-4 py-1 ml-2 '>
                {isEditing ? 'edit' : 'new item'}
            </Button>
        </form>
    );
};

export default Form;
