import React from 'react';
import { MdModeEdit } from 'react-icons/md';
import { FaTrash } from 'react-icons/fa';
import styled from 'styled-components';

const Item = styled.div`
    @keyframes move {
        0% {
            opacity: 0;
            top: -15px;
        }
        100% {
            opacity: 1;
            top: 0;
        }
    }
    animation: move 0.5s;
`;

const Items = ({ lists, removeItem, editItem }) => {
    if (lists.length === 0) {
        return <h4 className='text-xl font-bold mt-5'>No items</h4>;
    }
    return (
        <>
            {lists.map(({ id, title }) => (
                <Item
                    key={id}
                    className=' my-4 px-2 py-2 bg-gray-300 flex justify-between w-full items-center rounded-lg'
                >
                    <span className='text-gray-900'>{title}</span>
                    <div className='flex'>
                        <MdModeEdit
                            onClick={() => editItem(id)}
                            className='text-green-600'
                        />
                        <FaTrash
                            onClick={() => removeItem(id)}
                            className='text-red-700 ml-2'
                        />
                    </div>
                </Item>
            ))}
        </>
    );
};

export default Items;
