import React, { useEffect, useState } from 'react';
import Form from './Form';
import { v4 as uuidv4 } from 'uuid';
import Items from './Items';
import Alert from './Alert';

const getLocalStorage = () => {
    let list = localStorage.getItem('list');
    if (list) {
        return JSON.parse(localStorage.getItem('list'));
    } else {
        return [];
    }
};

const ListItems = () => {
    const [text, setText] = useState('');
    const [list, setList] = useState(getLocalStorage());
    const [alert, setAlert] = useState({ show: false, msg: '', type: '' });
    const [editId, setEditId] = useState(false);
    const [isEditing, setIsEditing] = useState(false);
    const handleSubmit = (e) => {
        e.preventDefault();
        if (!text) {
            showAlert(true, 'Please enter text', 'danger');
        } else if (text && isEditing) {
            setList(
                list.map((item) => {
                    if (item.id === editId) {
                        return { ...item, title: text };
                    }
                    return item;
                }),
            );
            showAlert(true, 'Items edited', 'success');
            setText('');
            setEditId(null);
            setIsEditing(false);
        } else {
            showAlert(true, 'Items added', 'success');
            const newItem = { id: uuidv4(), title: text };
            setList([...list, newItem]);
            setText('');
        }
    };
    const handleChangeText = (e) => {
        setText(e.target.value);
    };
    const clearList = () => {
        showAlert(true, 'Items clear', 'danger');
        setList([]);
    };
    const showAlert = (show = false, msg = '', type = '') => {
        setAlert({ show, msg, type });
    };
    const removeItem = (id) => {
        const newItems = list.filter((item) => item.id !== id);
        setList(newItems);
    };
    const editItem = (id) => {
        const itemIdEdit = list.find((item) => item.id === id);
        setIsEditing(true);
        setEditId(id);
        setText(itemIdEdit.title);
    };

    useEffect(() => {
        localStorage.setItem('list', JSON.stringify(list));
    }, [list]);
    return (
        <div className='flex mx-auto max-w-2xl mt-20'>
            <div className='bg-white px-4 py-4 w-full flex flex-col items-center rounded-lg'>
                {alert.show && (
                    <Alert {...alert} removeAlert={showAlert} list={list} />
                )}
                <h1 className='text-2xl font-bold'>List items</h1>
                <Form
                    text={text}
                    handleSubmit={handleSubmit}
                    handleChangeText={handleChangeText}
                    isEditing={isEditing}
                />

                {list.length > 0 && (
                    <>
                        <div
                            className={`w-full max-h-96 ${
                                list.length >= 7 && 'overflow-y-scroll'
                            }`}
                        >
                            <Items
                                lists={list}
                                removeItem={removeItem}
                                editItem={editItem}
                            />
                        </div>

                        <button className='mt-4' onClick={clearList}>
                            clear items
                        </button>
                    </>
                )}
            </div>
        </div>
    );
};

export default ListItems;
