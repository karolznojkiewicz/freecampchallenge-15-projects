import React from 'react';

import Button from '../Button/Button';

const List = ({ birthday, setBirthday }) => {
    return (
        <div className='bg-gray-50 rounded-md shadow-xl w-96 px-4 py-3 flex flex-col justify-center tracking-wider'>
            <h3 className='text-xl mb-3'>{birthday.length} birthdays today</h3>
            <div>
                {birthday.map(({ id, image, name, age }) => (
                    <div key={id} className='flex my-5 items-center'>
                        <img
                            className='w-24 h-24 rounded-full object-cover'
                            src={image}
                            alt=''
                        />
                        <div className='flex flex-col ml-4'>
                            <span className='font-bold text-lg'>{name}</span>
                            <span className='text-gray-600'>{age} years</span>
                        </div>
                    </div>
                ))}
            </div>
            <Button onClick={() => setBirthday([])} className='py-1 mt-3'>
                Clear All
            </Button>
        </div>
    );
};

export default List;
