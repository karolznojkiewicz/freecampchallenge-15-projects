import React, { useState } from 'react';
import List from './List';

import { data } from './../../Data/birthdayList';

const Birthday = () => {
    const [birthday, setBirthday] = useState(data);
    return (
        <div className='flex justify-center items-center'>
            <List birthday={birthday} setBirthday={setBirthday} />
        </div>
    );
};

export default Birthday;
