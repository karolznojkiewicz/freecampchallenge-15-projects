import React, { useState } from 'react';
import Button from '../Button/Button';
import Heading from '../Heading/Heading';
import people from './../../Data/reviews';
import { FaChevronLeft, FaChevronRight, FaQuoteRight } from 'react-icons/fa';

const Reviews = () => {
    const [index, setIndex] = useState(0);
    const { name, image, text, job } = people[index];

    const checkNumber = (number) => {
        if (number > people.length - 1) {
            return 0;
        }
        if (number < 0) {
            return people.length - 1;
        }
        return number;
    };

    const prevPerson = () => {
        setIndex((index) => {
            let newIndex = index - 1;
            return checkNumber(newIndex);
        });
    };
    const nextPerson = () => {
        setIndex((index) => {
            let newIndex = index + 1;
            return checkNumber(newIndex);
        });
    };

    const randomPerson = () => {
        let randomNumber = Math.floor(Math.random() * people.length);
        if (randomNumber === index) {
            randomNumber = index + 1;
        }
        setIndex(checkNumber(randomNumber));
    };
    return (
        <div className='flex flex-col justify-center items-center h-screen'>
            <Heading>Our Reviews</Heading>

            <article className='mt-16 max-w-xl py-7 px-5 flex flex-col items-center justify-center shadow-xl bg-gray-100 rounded-sm'>
                <div className='rounded-full w-40 h-40 relative bg-blue-500 '>
                    <img
                        className='rounded-full w-40 h-40 object-cover absolute right-2 top-1 z-10'
                        src={image}
                        alt={name}
                    />
                    <span className='bg-blue-500 grid place-items-center rounded-full w-10 h-10 z-50 absolute top-4 left-0 text-white'>
                        <FaQuoteRight />
                    </span>
                </div>
                <h3 className='mt-5 font-bold text-lg'>{name}</h3>
                <h4 className='uppercase text-blue-400 mb-4'>{job}</h4>
                <p className='text-center text-blue-900'>{text}</p>
                <div className='flex justify-between w-16 my-5 text-blue-400 text-xl'>
                    <div className='cursor-pointer' onClick={prevPerson}>
                        <FaChevronLeft />
                    </div>
                    <div
                        className='text-blue-400 text-xl cursor-pointer'
                        onClick={nextPerson}
                    >
                        <FaChevronRight />
                    </div>
                </div>
                <Button onClick={randomPerson} className='py-1 px-2'>
                    Suprise me
                </Button>
            </article>
        </div>
    );
};

export default Reviews;
