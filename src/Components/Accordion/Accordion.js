import React, { useState } from 'react';
import dataQuestions from './../../Data/questions';
import QuetionList from './QuetionList';

const Accordion = () => {
    const [questions, setQuestions] = useState(dataQuestions);

    return (
        <main className='flex justify-center items-center max-w-4xl mx-auto my-20 px-6 sm:px-0 '>
            <div className='flex flex-col justify-between md:flex-row bg-gray-50 px-10 py-10 rounded-xl h-2/3 w-full'>
                <div className='flex px-12 w-1/3'>
                    <h3 className='text-3xl font-semibold text-center'>
                        Questions and answear
                    </h3>
                </div>
                <section className='w-2/3'>
                    {questions.map(({ id, title, info }) => (
                        <QuetionList key={id} title={title} info={info} />
                    ))}
                </section>
            </div>
        </main>
    );
};

export default Accordion;
