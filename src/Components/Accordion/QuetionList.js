import React, { useState } from 'react';
import { BiPlus, BiMinus } from 'react-icons/bi';

const QuetionList = ({ title, info }) => {
    const [showInfo, setShowinfo] = useState(false);

    return (
        <article className='py-4 px-6 bg-gray-50 shadow-lg mb-6 transition-opacity '>
            <header className='flex justify-between'>
                <h4 className='font-bold'>{title}</h4>
                <button
                    onClick={() => setShowinfo(!showInfo)}
                    className='text-white text-xl shadow-inner w-8 h-8 bg-gray-200 rounded-full  flex items-center justify-center'
                >
                    {showInfo ? <BiMinus /> : <BiPlus />}
                </button>
            </header>
            <p className='pr-9 mt-3'>{showInfo && info}</p>
        </article>
    );
};

export default QuetionList;
