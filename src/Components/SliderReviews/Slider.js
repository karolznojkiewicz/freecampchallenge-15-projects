import React from 'react';
import { AiOutlineArrowLeft, AiOutlineArrowRight } from 'react-icons/ai';
import { FaQuoteRight } from 'react-icons/fa';

const Slider = ({
    image,
    name,
    title,
    quote,
    handleChangeNext,
    handleChangePrev,
}) => {
    return (
        <div className='flex justify-center items-center mt-20'>
            <div className='max-w-3xl flex items-center'>
                <button
                    onClick={handleChangePrev}
                    className='w-9 h-9 bg-blue-500 flex justify-center items-center text-gray-50 rounded-lg'
                >
                    <AiOutlineArrowLeft />
                </button>
                <div className='relative flex flex-col items-center '>
                    <div className='relative bg-blue-500 w-36 h-36 rounded-full shadow-lg'>
                        <img
                            className='w-36 h-36 rounded-full object-cover absolute top-1 right-1 shadow-lg'
                            src={image}
                            alt=''
                        />
                    </div>
                    <h4 className='text-2xl font-bold text-blue-800 mt-8 uppercase'>
                        {name}
                    </h4>
                    <h5 className='text-lg capitalize'>{title}</h5>
                    <p className='mt-10 text-center text-gray-600'>{quote}</p>
                    <FaQuoteRight className='text-6xl text-blue-900 mt-4' />
                </div>
                <button
                    onClick={handleChangeNext}
                    className='w-9 h-9 bg-blue-500 flex justify-center items-center text-gray-50 rounded-lg'
                >
                    <AiOutlineArrowRight />
                </button>
            </div>
        </div>
    );
};

export default Slider;
