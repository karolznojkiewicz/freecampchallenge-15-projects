import React, { useEffect, useState } from 'react';
import Heading from '../Heading/Heading';
import dataPeople from './../../Data/SliderReviews';
import Slider from './Slider';

const SliderReviews = () => {
    const [people] = useState(dataPeople);
    const [value, setValue] = useState(0);

    const { image, name, title, quote } = people[value];

    const handleChangeNext = () => {
        setValue((value) => {
            let newPeople = value + 1;
            if (newPeople > people.length - 1) {
                return 0;
            }
            return newPeople;
        });
    };

    const handleChangePrev = () => {
        setValue((value) => {
            let newPeople = value - 1;
            if (newPeople < 0) {
                return people.length - 1;
            }
            return newPeople;
        });
    };

    useEffect(() => {
        setInterval(() => {
            setValue((value) => {
                let newPeople = value + 1;
                if (newPeople > people.length - 1) {
                    return 0;
                }
                return newPeople;
            });
        }, 2000);
    }, [people.length]);

    return (
        <>
            <Heading>/Reviews</Heading>
            <Slider
                image={image}
                name={name}
                title={title}
                quote={quote}
                handleChangeNext={handleChangeNext}
                handleChangePrev={handleChangePrev}
            />
        </>
    );
};

export default SliderReviews;
