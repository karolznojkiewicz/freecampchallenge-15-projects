import React from 'react';

const MenuList = ({ menu, firstLetterUppper }) => {
    return (
        <div className='grid grid-cols-2 gap-3 grid-row auto-rows-auto mx-8'>
            {menu.map(({ id, title, category, price, img, desc }) => (
                <div key={id} className='flex mt-9'>
                    <div className='w-1/3 h-48  shadow-inner'>
                        <img
                            className='w-full h-full object-cover bg-top'
                            src={img}
                            alt={title}
                        />
                    </div>
                    <div className='flex flex-col px-4 w-2/3'>
                        <header className='flex justify-between border-b-2 mb-3'>
                            <h4 className='text-lg font-bold font'>
                                {firstLetterUppper(title)}
                            </h4>
                            <span className='text-gray-600 text-lg font-bold'>
                                $ {price}
                            </span>
                        </header>
                        <p className='py-2 pr-10'>{firstLetterUppper(desc)}</p>
                    </div>
                </div>
            ))}
        </div>
    );
};

export default MenuList;
