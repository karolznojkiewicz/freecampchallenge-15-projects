import React, { useState } from 'react';
import Heading from '../Heading/Heading';
import menuData from './../../Data/menu';
import Categories from './Categories';
import MenuList from './MenuList';

const Menu = () => {
    const [menu, setMenu] = useState(menuData);

    const firstLetterUppper = (text) => {
        let str = text.slice(0, 1).toUpperCase();
        let strOther = text.slice(1, text.length - 1);
        let modyfied = str + strOther;

        return modyfied;
    };

    const allCategories = [
        'All',
        ...new Set(menu.map((item) => item.category)),
    ];

    const [categories] = useState(allCategories);

    const filerCategories = (category) => {
        if (category === 'All') {
            setMenu(menuData);
            return;
        }
        const newCategory = menuData.filter(
            (item) => item.category === category,
        );
        setMenu(newCategory);
    };

    return (
        <>
            <Heading>Our Menu</Heading>

            <Categories
                categories={categories}
                filerCategories={filerCategories}
            />
            <MenuList menu={menu} firstLetterUppper={firstLetterUppper} />
        </>
    );
};

export default Menu;
