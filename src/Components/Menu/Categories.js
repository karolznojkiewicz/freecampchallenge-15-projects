import React from 'react';

const Categories = ({ categories, filerCategories }) => {
    return (
        <div className='flex justify-center my-10'>
            {categories.map((category, index) => (
                <button
                    key={index}
                    className='mr-4'
                    onClick={() => filerCategories(category)}
                >
                    {category}
                </button>
            ))}
        </div>
    );
};

export default Categories;
