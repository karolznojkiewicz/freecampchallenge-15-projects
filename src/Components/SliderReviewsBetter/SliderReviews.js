import React, { useEffect, useState } from 'react';
import Heading from '../Heading/Heading';
import dataPeople from './../../Data/SliderReviews';
import { AiOutlineArrowLeft, AiOutlineArrowRight } from 'react-icons/ai';
import styled from 'styled-components';

const Article = styled.article`
    transition: all 0.3s linear;
    &.activeSlider {
        opacity: 1;
        transform: translateX(0);
    }
    &.lastSlider {
        transform: translateX(-100%);
    }
    &.nextSlider {
        transform: translateX(100%);
    }
`;

const SliderReviews = () => {
    const [people] = useState(dataPeople);
    const [index, setIndex] = useState(0);
    const lastItem = people.length - 1;
    const handleChangeNext = () => {
        setIndex((index) => {
            let newPeople = index + 1;
            if (newPeople > lastItem) {
                return 0;
            }
            return newPeople;
        });
    };

    const handleChangePrev = () => {
        setIndex((index) => {
            let newPeople = index - 1;
            if (newPeople < 0) {
                return lastItem;
            }
            return newPeople;
        });
    };

    useEffect(() => {
        const slider = setInterval(() => {
            setIndex((index) => {
                let newPeople = index + 1;
                if (newPeople > lastItem) {
                    return 0;
                }
                return newPeople;
            });
        }, 4000);
        return () => clearInterval(slider);
    }, [lastItem]);

    return (
        <section className='max-w-7xl mx-auto my-20 overflow-hidden'>
            <div>
                <Heading>Reviews</Heading>
            </div>
            <div className='mx-auto max-w-4xl h-96 text-center relative flex'>
                {people.map((person, personIndex) => {
                    const { id, image, name, title, quote } = person;
                    let position = 'nextSlider';
                    if (personIndex === index) {
                        position = 'activeSlider';
                    }
                    if (
                        personIndex === index - 1 ||
                        (index === 0 && personIndex === person.length - 1)
                    ) {
                        position = 'lastSlider';
                    }
                    if (personIndex === index + 1) {
                        position = 'nextSlider';
                    }
                    return (
                        <>
                            <button
                                onClick={handleChangePrev}
                                className='absolute left-0 top-48 w-9 h-9 bg-blue-500 flex justify-center items-center text-gray-50 rounded-lg'
                            >
                                <AiOutlineArrowLeft />
                            </button>
                            <Article
                                key={id}
                                className={`${position} flex flex-col items-center absolute top-0 left-0 w-full h-full opacity-0`}
                            >
                                <img
                                    className='w-36 h-36 rounded-full object-cover shadow-lg mb-4'
                                    src={image}
                                    alt={name}
                                />
                                <h4 className='text-2xl font-bold text-blue-800 mb-1 uppercase'>
                                    {name}
                                </h4>
                                <p className="'text-lg capitalize mb-3">
                                    {title}
                                </p>
                                <p className='mt-8 leading-tight text-gray-600'>
                                    {quote}
                                </p>
                            </Article>
                            <button
                                onClick={handleChangeNext}
                                className='absolute right-0 top-48 w-9 h-9 bg-blue-500 flex justify-center items-center text-gray-50 rounded-lg'
                            >
                                <AiOutlineArrowRight />
                            </button>
                        </>
                    );
                })}
            </div>
        </section>
    );
};

export default SliderReviews;
