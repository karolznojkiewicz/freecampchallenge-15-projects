import React from 'react';
import { useEffect, useState } from 'react';
import Button from '../Button/Button';
import Heading from '../Heading/Heading';
import ListTour from './ListTour';
import Loading from './Loading';

const API_CODE = 'https://course-api.com/react-tours-project';

const Turist = () => {
    const [loading, setLoading] = useState(true);
    const [tours, setTours] = useState([]);

    const fetchTours = async () => {
        await fetch(API_CODE)
            .then((res) => res.json())
            .then((data) => {
                setLoading(false);
                setTours(data);
            })
            .catch((e) => {
                setLoading(false);
                console.log(e);
            });
    };

    useEffect(() => {
        fetchTours();
    }, []);

    const filterTurists = (id) => {
        const newTours = tours.filter((tour) => tour.id !== id);
        setTours(newTours);
    };

    if (loading) {
        return (
            <main>
                <Loading />
            </main>
        );
    }
    if (tours.length === 0) {
        return (
            <main className='flex justify-center items-center h-screen'>
                <Button className='py-4 px-6' onClick={fetchTours}>
                    Refresh
                </Button>
            </main>
        );
    }
    return (
        <main className='mx-auto max-w-2xl '>
            <Heading>Our Turist</Heading>
            <ListTour tours={tours} filterTurists={filterTurists} />
        </main>
    );
};

export default Turist;
