import React, { useState } from 'react';
import Button from '../Button/Button';

const ListTour = ({ tours, filterTurists }) => {
    const [readMore, setReadMore] = useState(false);

    return (
        <>
            {tours.map(({ id, name, info, image, price }) => (
                <>
                    <div
                        key={id}
                        className='flex flex-col my-10 bg-gray-50 rounded-md'
                    >
                        <img
                            className='h-80 object-cover'
                            src={image}
                            alt={name}
                        />
                        <div className='p-8'>
                            <div className='flex justify-between items-center'>
                                <span className='font-extrabold'>{name}</span>
                                <span className='bg-blue-300 p-2 rounded-md font-bold text-white'>
                                    ${price}
                                </span>
                            </div>
                            <p className='my-6 text-gray-700 leading-relaxed'>
                                {readMore
                                    ? info
                                    : `${info.substring(0, 200)}...`}
                                <button onClick={() => setReadMore(!readMore)}>
                                    {readMore ? 'Show less' : 'read more'}
                                </button>
                            </p>
                            <Button
                                onClick={() => filterTurists(id)}
                                className='w-full mx-auto py-3 px-10'
                            >
                                Not intrested
                            </Button>
                        </div>
                    </div>
                </>
            ))}
        </>
    );
};

export default ListTour;
