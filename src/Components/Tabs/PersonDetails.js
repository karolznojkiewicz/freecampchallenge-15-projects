import React from 'react';
import { FaLocationArrow } from 'react-icons/fa';
import Button from '../Button/Button';

const PersonDetails = ({ company, title, dates, duties }) => {
    return (
        <div className='max-w-5xl flex flex-col'>
            <div>
                <h3 className='text-3xl text-gray-800 mb-3'>{title}</h3>
            </div>
            <div>
                <span className='bg-gray-400 text-gray-700 py-1 px-2 font-bold'>
                    {company}
                </span>
            </div>
            <div className='my-3'>
                <span className='font-semibold text-gray-500 text-lg'>
                    {dates}
                </span>
            </div>
            <ul>
                {duties.map((item, index) => (
                    <li key={index} className=''>
                        <span className='mr-6'>
                            <FaLocationArrow />
                        </span>
                        {item}
                    </li>
                ))}
            </ul>
            <Button className='mt-20 w-52 py-1'>More info</Button>
        </div>
    );
};

export default PersonDetails;
