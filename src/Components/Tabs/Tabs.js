import React, { useEffect, useState } from 'react';
import companyData from '../../Data/companyData';
import Heading from '../Heading/Heading';
import PersonDetails from './PersonDetails';

const Tabs = () => {
    const [jobs, setJobs] = useState(companyData);
    const [value, setValue] = useState(0);

    const { title, dates, duties, company } = jobs[value];

    return (
        <div>
            <Heading>Expierence</Heading>
            <div className='mt-32 flex flex-col md:flex-row max-w-7xl mx-auto'>
                <ul className='w-1/4 '>
                    {jobs.map(({ company }, index) => (
                        <button
                            key={index}
                            className={`flex items-center font-normal text-xl my-7 border-l-4 border-transparent pl-3 ${
                                index === value &&
                                'font-semibold border-l-4 border-blue-800'
                            }`}
                            onClick={() => setValue(index)}
                        >
                            {company}
                        </button>
                    ))}
                </ul>
                <PersonDetails
                    title={title}
                    dates={dates}
                    duties={duties}
                    company={company}
                />
            </div>
        </div>
    );
};

export default Tabs;
