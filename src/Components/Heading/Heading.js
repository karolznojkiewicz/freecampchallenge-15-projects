import React from 'react';

const Heading = ({ children }) => {
    return (
        <h1 className='text-center text-5xl font-bold mt-9 mb-6'>{children}</h1>
    );
};

export default Heading;
