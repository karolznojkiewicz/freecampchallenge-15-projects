import React, { useState } from 'react';
import Button from '../Button/Button';
import Values from 'values.js';

const ColorGenerator = () => {
    const [color, setColor] = useState('');
    const [error, setError] = useState(false);
    const [list, setList] = useState(new Values('#f15a11').all(10));
    const [alert, setAlert] = useState(false);

    const handleSubmit = (e) => {
        e.preventDefault();
        try {
            let colors = new Values(color).all(10);
            setList(colors);
        } catch {
            setError(true);
            setColor('Nieprawidłowy kolor');
        }
    };
    const handleChangeColor = (e) => {
        setColor(e.target.value);
    };

    const componentToHex = (c) => {
        let hex = c.toString(16);

        return hex.length === 1 ? '0' + hex : hex;
    };

    const rgbaToHex = (r = 0, g = 0, b = 0) => {
        return '#' + componentToHex(r) + componentToHex(g) + componentToHex(b);
    };

    return (
        <>
            <div className='max-w-4xl mx-auto flex flex-col items-center'>
                <div className='flex mx-28 py-20'>
                    <h1 className='text-xl mr-4'>Color Generator</h1>
                    <form onSubmit={handleSubmit}>
                        <input
                            type='text'
                            value={color}
                            onChange={handleChangeColor}
                            placeholder='#000'
                            className={`${
                                error
                                    ? 'border-2 border-red-600 text-red-800'
                                    : 'border-0 text-gray-900'
                            }`}
                        />
                        <Button className='ml-3 px-4 py-1' type='submit'>
                            Submit
                        </Button>
                    </form>
                </div>
                <section className='flex flex-wrap'>
                    {list.map((color, index) => {
                        const bcg = color.rgb.join(', ');
                        const hex = rgbaToHex(...bcg);

                        return (
                            <div
                                key={index}
                                style={{
                                    backgroundColor: `rgb(${bcg})`,
                                }}
                                className='w-44 h-32'
                                onClick={() => {
                                    setAlert(true);
                                    navigator.clipboard.writeText(hex);
                                }}
                            >
                                <div
                                    className={`flex flex-col items-center justify-center ${
                                        index > 10
                                            ? 'text-white'
                                            : 'text-gray-900'
                                    }`}
                                >
                                    <span>{color.weight}%</span>
                                    <span>{hex}</span>
                                    {alert && (
                                        <span className='mt-5 text-green-800'>
                                            copied to clipboard
                                        </span>
                                    )}
                                </div>
                            </div>
                        );
                    })}
                </section>
            </div>
        </>
    );
};

export default ColorGenerator;
