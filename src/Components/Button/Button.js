import React from 'react';

const Button = ({ onClick, className, children, type }) => {
    return (
        <button
            type={type}
            className={`rounded bg-blue-700 text-gray-50 ${className}`}
            onClick={onClick}
        >
            {children}
        </button>
    );
};

export default Button;
