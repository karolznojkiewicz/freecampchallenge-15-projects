const companyData = [
    {
        order: 3,
        title: 'Full Stack Web Developer',
        dates: 'December 2015 - Present',
        duties: ['Duties 1', 'Duties 2', 'Duties 3'],
        company: 'Tommy',
    },
    {
        order: 2,
        title: 'Front-End Enginner',
        dates: 'May 2015 - December 2015',
        duties: ['Duties 1', 'Duties 2', 'Duties 3'],
        company: 'Bigdrop',
    },
    {
        order: 1,
        title: 'Engineering Intern',
        dates: 'May 2014 - Semtember 2015',
        duties: ['Duties 1', 'Duties 2', 'Duties 3'],
        company: 'Cukker',
    },
];
export default companyData;
